CREATE TABLE `teachers` (
`id` int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
`name` varchar(250) NOT NULL,
`avatar` varchar(250) NOT NULL,
`description` text DEFAULT NULL,
`specialized` char(10) NOT NULL,
`degree` char(10) NOT NULL,
`updated` datetime NOT NULL,
`created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `degree`(
	`id` char(10) NOT NULL,
    `name` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `degree`(`id`, `name`) 
VALUES ('001','Cử nhân');

INSERT INTO `degree`(`id`, `name`) 
VALUES ('002','Thạc sĩ');

INSERT INTO `degree`(`id`, `name`) 
VALUES ('003','Tiến sĩ');

INSERT INTO `degree`(`id`, `name`) 
VALUES ('004','Phó giáo sư');

INSERT INTO `degree`(`id`, `name`) 
VALUES ('005','Giáo sư');
