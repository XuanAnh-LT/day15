<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset='UTF-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <link rel='stylesheet' href='teacher_search.css'>
    <title>Teacher_search</title>
</head>

<?php
$servername = "localhost";
$database = "teachers";
$username = "root";
$password = "";
// Create connection
$conn = mysqli_connect($servername, $username, $password, $database);

mysqli_set_charset($conn, "utf8");

$sql = "SELECT id, name, avatar, description, specialized, degree, updated, created FROM teachers";
$result = $conn->query($sql);
?>

<body>
<?php 
     session_start();
     
     if (isset($_POST['search'])) 
                     {  
                        if(isset($_POST['search_subject'])){
                            $_SESSION['search_subject']  = $_POST['search_subject'];
                            $keysubject = $_POST['search_subject'];
                            
                        }
                        if(isset($_POST['search_keyword'])){
                            $_SESSION['search_keyword']  = $_POST['search_keyword'];
                            $keyword = $_POST['search_keyword'];

                        }
                     }
    ?>


<form action='' method='POST'>
    <div class='container_margin'>
        <table class='search_box' cellspacing='15'>
            <tr>
                <td class='search_box_row'>Khoa:</td>
                <td><select class='box' id='search_box_subject' name='search_subject' value = ''>
                <?php
                $subject = array("" => "", "001" => "Khoa học máy tính", "002" => "Khoa học dữ liệu", "003" => "Hải dương học");
                            foreach ($subject as $key => $value) {
                            echo "
                            <option ";
                            echo isset($_SESSION['search_subject']) && $_SESSION['search_subject'] == $key ? "selected " : "";
                            echo "  value='" . $key . "'>" . $value  . "</option>";
                            }
                $degree = array("" => "", "001" => "Cử nhân", "002" => "Thạc sĩ", "003" => "Tiến sĩ", "004" => "Phó giáo sư", "005" => "Giáo sư")
                ?>
                </td> 
            </tr>
    
            <tr>
                <td class='search_box_row'>Từ khóa:</td>
                <td><input type='text' class='box' id='search_box_keyword' name='search_keyword' value = <?php echo isset($_SESSION['search_keyword']) ? $_SESSION['search_keyword'] : '' ?> ></td>
            </tr>
        </table>

        <br>

        <div class='align_center'>
            <input class='button_search' type='submit' name='search' value='Tìm kiếm'></input>
        </div>

        <br><br><br>
        
        <?php 
            if(!isset($_POST['search'])){
                $query = mysqli_query($conn, "SELECT * FROM `teachers`");
                $rowcount = mysqli_num_rows( $query );
                echo "<label id='count_record' style='text-align: left;'>Số giáo viên tìm thấy: ".$rowcount." </label>";

            } else{
                echo "
                    <script>
                        document.getElementById('count_record').remove();
                    </script>
                ";
                if(empty($_POST['search_subject'])){
                    // $query = mysqli_query($conn, "SELECT * FROM `teachers` WHERE  `name` LIKE '%$keyword%'  
                    //                                                         or `description` LIKE '%$keyword%' 
                    //                                                         or `degree` LIKE '%$keyword%'");

                    $query = mysqli_query($conn, "SELECT teachers.* FROM `teachers` INNER JOIN  `degree`   on teachers.degree = degree.id
                                                                                    WHERE teachers.name LIKE '%$keyword%'  
                                                                                    or teachers.description LIKE '%$keyword%' 
                                                                                    or teachers.degree LIKE '%$keyword%' 
                                                                                    or degree.name LIKE '%$keyword%' ");
                    $rowcount = mysqli_num_rows( $query );
                    echo "<label id='count_record' style='text-align: left;'>Số giáo viên tìm thấy: ".$rowcount." </label>";
                
                } else{
                    // $query = mysqli_query($conn, "SELECT * FROM `teachers` WHERE `specialized` = '$keysubject'  and  `name` LIKE '%$keyword%'  
                    //                                                         or `specialized` = '$keysubject'  and `description` LIKE '%$keyword%' 
                    //                                                         or `specialized` = '$keysubject' and `degree` LIKE '%$keyword%'");

                    $query = mysqli_query($conn, "SELECT teachers.* FROM `teachers` INNER JOIN `degree` on teachers.degree = degree.id
                                                                                    WHERE teachers.specialized = '$keysubject'  and  teachers.name LIKE '%$keyword%'  
                                                                                    or teachers.specialized = '$keysubject'  and teachers.description LIKE '%$keyword%' 
                                                                                    or teachers.specialized = '$keysubject' and teachers.degree LIKE '%$keyword%'
                                                                                    or teachers.specialized = '$keysubject' and degree.name LIKE '%$keyword%'");
                    $rowcount = mysqli_num_rows( $query );
                    echo "<label id='count_record' style='text-align: left;'>Số giáo viên tìm thấy: ".$rowcount." </label>";
                }
                // $rowcount = mysqli_num_rows( $query );
                // echo "<label id='count_record' style='text-align: left;'>Số giáo viên tìm thấy:".$rowcount." </label>";
            }

        ?>

        <!-- <label style='text-align: left;'>Số giáo viên tìm thấy: 
            <?php //echo $rowcount?>
        </label> -->


        <br><br>

        <table  style='width: 100%'>
            <thead>
                <td style='width: 5%'>No</td>
                <td style='width: 20%'>Tên giáo viên</td>
                <td style='width: 20%'>Khoa</td>
                <td style='width: 32%'>Mô tả chi tiết</td>
                <td style='width: 20%'>Action</td>
            </thead>

            <tbody>
            <?php
                if ($result->num_rows > 0) {

                    if (isset($_POST['search'])) {
                        if(empty($_POST['search_subject'])){
                            $query = mysqli_query($conn, "SELECT teachers.* FROM `teachers` 
                                                                            INNER JOIN  `degree`   on teachers.degree = degree.id
                                                                            WHERE teachers.name LIKE '%$keyword%'  
                                                                            or teachers.description LIKE '%$keyword%' 
                                                                            or teachers.degree LIKE '%$keyword%' 
                                                                            or degree.name LIKE '%$keyword%' ");
                            $rowcount = mysqli_num_rows( $query );
                            // }

                            while($row = mysqli_fetch_array($query)){ 
                                    echo "<tr>
                                        <td >
                                                <p>" . $row["id"]. "</p>
                                            </td>
                                            <td>
                                                <p class='display'>" . $row["name"]. "</p>
                                            </td>  
                                            <td>";
                                            foreach ($subject as $key => $value){
                                                if($row["specialized"] == $key)
                                                echo"
                                                <p class='display'>". $value. "</p>
                                            </td>";
                                            }
                                            echo "
                                            <td>
                                                <p class='display'>". $row["description"]. "</p>
                                            </td>
                                            <td>
                                            <div class='display_flex'>
                                                <input type='button' id='"; echo $row['id']; echo"' class = 'custom_del' value='Xóa'/>
                                                <div class = 'edit_button'>
                                                    <a class='custom_a' href='edit.php?id="; echo $row['id']; echo"'>Sửa</a>
                                                </div>
                                            </div>

                                            </td>
                                        <script >
                                                var button = document.getElementById('"; echo $row['id']; echo"');
                                                button.onclick = function(){
                                                    var result = confirm('Bạn chắc chắn muốn xóa giáo viên ".$row["name"]." ?');
                                                        if (result == true) {
                                                            window.location ='delete.php?id="; echo $row['id']; echo"'
                                                        } else {
                                                            alert('Bạn đã hủy xóa giáo viên');
                                                        }
                                                }
                                            </script>
                                        </tr>
                                            ";
                                    
                            }
                        }
                    $query = mysqli_query($conn, "SELECT teachers.* FROM `teachers` INNER JOIN `degree` on teachers.degree = degree.id
                                                                            WHERE teachers.specialized = '$keysubject'  and  teachers.name LIKE '%$keyword%'  
                                                                            or teachers.specialized = '$keysubject'  and teachers.description LIKE '%$keyword%' 
                                                                            or teachers.specialized = '$keysubject' and teachers.degree LIKE '%$keyword%'
                                                                            or teachers.specialized = '$keysubject' and degree.name LIKE '%$keyword%'");
                    $rowcount = mysqli_num_rows( $query );
                        while($row = mysqli_fetch_array($query)){ 
                                echo "<tr>
                                       <td >
                                            <p>" . $row["id"]. "</p>
                                        </td>
                                        <td>
                                            <p class='display'>" . $row["name"]. "</p>
                                        </td>  
                                        <td>";
                                        foreach ($subject as $key => $value){
                                            if($row["specialized"] == $key)
                                            echo"
                                            <p class='display'>". $value. "</p>
                                        </td>";
                                        }
                                        echo "
                                        <td>
                                            <p class='display'>". $row["description"]. "</p>
                                        </td>
                                        <td>
                                        <div class='display_flex'>
                                            <input type='button' id='"; echo $row['id']; echo"' class = 'custom_del' value='Xóa'/>
                                            <div class = 'edit_button'>
                                                <a class='custom_a' href='edit.php?id="; echo $row['id']; echo"'>Sửa</a>
                                            </div>
                                        </div>
                                        </td>
                                    <script >
                                            var button = document.getElementById('"; echo $row['id']; echo"');
                                            button.onclick = function(){
                                                var result = confirm('Bạn chắc chắn muốn xóa giáo viên ".$row["name"]." ?');
                                                    if (result == true) {
                                                        window.location ='delete.php?id="; echo $row['id']; echo"'
                                                    } else {
                                                        alert('Bạn đã hủy xóa giáo viên');
                                                    }
                                            }
                                        </script>
                                    </tr>
                                        ";
                                
                        }
                        
                    

                    }
                    // Load dữ liệu lên website
                    else{
                        while($row = $result->fetch_assoc()) {
                            echo "<tr>
                                   <td >
                                        <p>" . $row["id"]. "</p>
                                    </td>
                                    <td>
                                        <p class='display'>" . $row["name"]. "</p>
                                    </td>  
                                    <td>";
                                    foreach ($subject as $key => $value){
                                        if($row["specialized"] == $key)
                                        echo"
                                        <p class='display'>". $value. "</p>
                                    </td>";
                                    }
                                    echo "
                                    <td>
                                        <p class='display'>". $row["description"]. "</p>
                                    </td>
                                    <td>
                                    <div class='display_flex'>
                                        <input type='button' id='"; echo $row['id']; echo"' class = 'custom_del' value='Xóa'/>
                                        <div class = 'edit_button'>
                                            <a class='custom_a' href='edit.php?id="; echo $row['id']; echo"'>Sửa</a>
                                        </div>
                                    </div>
                                    </td>
                                    <script >
                                            var button = document.getElementById('"; echo $row['id']; echo"');
                                            button.onclick = function(){
                                                var result = confirm('Bạn chắc chắn muốn xóa giáo viên ".$row["name"]." ?');
                                                    if (result == true) {
                                                        window.location ='delete.php?id="; echo $row['id']; echo"'
                                                    } else {
                                                        alert('Bạn đã hủy xóa giáo viên');
                                                    }
                                            }
                                        </script>
                                </tr>
                                
                                    ";
                            }
                        }
                    
                    } 
                    $conn->close();
                    ?>
            </tbody>
        </table>
    
    </div>
</form>
    
<script>
            function ClearFields() {
                document.getElementById("keysubject").value = "";
                document.getElementById("keyword").value = "";
                sessionStorage.clear();
                }
</script>

<?php 
    session_destroy();
?>

</body>
</html>